﻿using Newtonsoft.Json.Linq;
using NorthwindSolution.Helpers;
using NorthwindSolution.IServices;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace NorthwindSolution.Controllers
{
  
    /// <summary>
    /// Implements Employee Controler
    /// </summary>
   // [RoutePrefix("api/Employee")]
    public class EmployeeController : ApiController
    {


        #region Private Variables
        private readonly IEmployeeService empService;
        #endregion

        #region Public Constructor
        public EmployeeController(IEmployeeService empService)
        {
            this.empService = empService;
        }
        #endregion

        #region Public Methods


        [AllowAnonymous]
        [HttpGet]
        [Route("api/data/forall")]
        public IHttpActionResult Get()
        {
            return Ok("Now server time is: " + DateTime.Now.ToString());
        }



        /// <summary>
        /// Get all employee details
        /// </summary>
        /// <returns>returns a list of employees</returns>
        [HttpGet]
        [Route("GetEmployees")]
        [Authorize]
        public HttpResponseMessage GetEmployees()
        {
            try
            {
                Mail.sendMail("wmpehanihansika@gmail.com","Get Employee");
                return Request.CreateResponse(HttpStatusCode.OK, empService.GetEmployees());
            }
            catch (Exception ex)
            {

                //use nLog to log error here
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
        } 
        #endregion
    }
}
