﻿using NorthwindSolution.RabbitMQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NorthwindSolution.Controllers
{
    public class RabbitMQController : ApiController
    {
        ConsumeRabbitMQ obj = new ConsumeRabbitMQ();
        [HttpGet]
        [Route("Consumer")]
        public HttpResponseMessage Consumer()
        {
            try
            {
                obj.ConsumeQueue();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
        }
    }
}
