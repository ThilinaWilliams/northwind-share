﻿using NorthwindSolution.Entities;
using NorthwindSolution.IServices;
using NorthwindSolution.Logging;
using NorthwindSolution.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NorthwindSolution.Controllers
{
    [RoutePrefix("api/Order")]
    public class OrderController : ApiController
    {
        #region Variables
        /// <summary>
        /// Get customer data from order service layer
        /// </summary>
        private readonly IOrderService order;

        //---------------Author: Chamod, Declared to use in updateOrders method-------//
        private readonly IOrderService orderDetails; 
        #endregion

        /// <summary>
        /// Assign data to order controller
        /// </summary>
        /// <param name="order"></param>
        public OrderController(IOrderService order, IOrderService orderDetails)
        {
            this.order = order;
            this.orderDetails = orderDetails;

        }

        /// <summary>
        /// Call SelectOrdersById method to select orders by EmployeeID
        /// </summary>
        /// <param name="customerid"></param>
        /// <returns></returns>
        [HttpGet]        
        [Route("Get_Orders_ById")]
        public HttpResponseMessage SelectOrdersById(string employeeid)
        {
            NLogErrorLog.LogInfoMessage("SelectById API is being called");
            try
            {                
                return Request.CreateResponse(HttpStatusCode.OK, order.SelectOrdersById(employeeid));
            }
            catch (Exception ex)
            {
                NLogErrorLog.LogErrorMessages(ex.Message);
                return Request.CreateResponse(HttpStatusCode.OK, "Something went wrong! Please Try Again!");
            }
        }

        [HttpPost]
        [Route("Update")]
        public HttpResponseMessage UpdateOrders(OrderDetails orders)
        {
            NLogErrorLog.LogInfoMessage("GetCustomers API is being called");
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, orderDetails.UpdateOrders(orders));
            }
            catch (Exception ex)
            {
                NLogErrorLog.LogErrorMessages(ex.Message);
                return Request.CreateResponse(HttpStatusCode.OK, "ERROR! Please Try Again");
            }
        }
    }
}
