﻿using Microsoft.IdentityModel.Tokens;
using NorthwindSolution.Entities;
using NorthwindSolution.IServices;
using NorthwindSolution.Logging;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web.Http;

namespace NorthwindSolution.Controllers
{
    //[RoutePrefix("api/CustomerOrderBitBucket")]
    ////[Authorize]
    //public class CustomerBitBucketController : ApiController
    //{
    //    //test
    //    #region Private Variables
    //    private readonly ICustomerBitBucketService customerService;
    //    #endregion

    //    #region public constructer
    //    public CustomerBitBucketController(ICustomerBitBucketService customerService)
    //    {
    //        this.customerService = customerService;
    //    }
    //    #endregion
    //    /// <summary>
    //    /// Customer Orders History Controller
    //    /// </summary>
    //    /// <param name="CustomerID"></param>
    //    /// <returns></returns>

    //    [HttpPost]
    //    [Route("CustomerOrderHistory")]
    //    public HttpResponseMessage GetCustomerOrderHistory(string CustomerID)
    //    {
    //        try
    //        {
    //            return Request.CreateResponse(HttpStatusCode.OK, customerService.GetCustomerOrderHistory(CustomerID));
    //        }
    //        catch (Exception ex)
    //        {
    //            return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
    //        }
    //    }

    //    /// <summary>
    //    /// Get customer details by Id
    //    /// </summary>
    //    /// <param name="customerid"></param>
    //    /// <returns></returns>
    //    [HttpGet]
    //    [Route("GetCustomersById")]
    //    public HttpResponseMessage SelectById(string customerid)
    //    {
    //        NLogErrorLog.LogInfoMessage("SelectById API is being called");
    //        try
    //        {
    //            return Request.CreateResponse(HttpStatusCode.OK, customerService.SelectById(customerid));
    //        }
    //        catch (Exception ex)
    //        {
    //            NLogErrorLog.LogErrorMessages(ex.Message);
    //            return Request.CreateResponse(HttpStatusCode.OK, "Something went wrong! Please Try Again!");
    //        }
    //    }

    //    /// <summary>
    //    /// Get details by CustOrdersDetail procedure
    //    /// </summary> 
    //    /// <param name="orderid"></param>
    //    /// <returns></returns> 
    //    [HttpGet]
    //    [Route("Get_Order_Detail_byID")]
    //    public HttpResponseMessage GetCustOrdersDetailbyId(string orderid)
    //    {
    //        NLogErrorLog.LogInfoMessage("GetCustOrdersDetailbyId API is being called");
    //        try
    //        {               
    //            return Request.CreateResponse(HttpStatusCode.OK, customerService.GetCustOrdersDetailbyId(orderid));
    //        }
    //        catch (Exception ex)
    //        {
    //            NLogErrorLog.LogErrorMessages(ex.Message);
    //            return Request.CreateResponse(HttpStatusCode.OK, "Something went wrong! Please Try Again!");
    //        }
    //    }

    //    /// <summary>
    //    /// Delete Customer Details
    //    /// </summary>
    //    /// <param name="CustomerID"></param>
    //    /// <returns></returns>
    //    [HttpDelete]
    //    [Route("DeleteCustomerDetails")]
    //    [AllowAnonymous]
    //    public HttpResponseMessage DeleteCustomerDetails(string CustomerID)
    //    {
    //        try
    //        {
    //            bool success = customerService.DeleteCustomerDetails(CustomerID);

    //            if(success)
    //            {
    //                return Request.CreateResponse(HttpStatusCode.OK, "Successfully deleted");
    //            }
    //            else
    //            {
    //                return Request.CreateResponse(HttpStatusCode.OK,"Failed to delete" );
    //            }
                    
               
    //        }
    //        catch (Exception ex)
    //        {

    //            return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
    //        }
    //    }

    //    /// <summary>
    //    /// ---------------------Get All Customers-----------///
    //    /// </summary>
    //    /// <returns></returns>
    //    [AllowAnonymous]
    //    [HttpGet]
    //    [Route("GetAllCustomers")]
    //    public HttpResponseMessage GetAllCustomres()
    //    {
    //        NLogErrorLog.LogInfoMessage("GetCustomers API is being called");
    //        try
    //        {
    //            return Request.CreateResponse(HttpStatusCode.OK, customerService.GetAllCustomers());
    //        }
    //        catch (Exception ex)
    //        {
    //            NLogErrorLog.LogErrorMessages(ex.Message);
    //            return Request.CreateResponse(HttpStatusCode.OK, "Something is not right! Please Try Again!");

    //        }
    //    }

    //    [HttpGet]
    //    [Route("GetOrderOrders")]
    //    public HttpResponseMessage GetOrderOrders(string customerID)
    //    {
    //        NLogErrorLog.LogInfoMessage("GetOrderDates API is beggin called");
    //        try
    //        {
    //            return Request.CreateResponse(HttpStatusCode.OK, customerService.OrderDates(customerID));
    //        }
    //        catch (Exception ex)
    //        {
    //            NLogErrorLog.LogErrorMessages(ex.Message);
    //            return Request.CreateResponse(HttpStatusCode.NotFound, "Something is not right! Please Try Again!");
    //        }
    //    }

    //}
}
