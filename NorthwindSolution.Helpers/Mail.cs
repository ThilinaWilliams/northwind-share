﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindSolution.Helpers
{
    public static class Mail
    {
        public static string sendMail(string To, string Body)
        {
            try
            {
                MailMessage mail = new MailMessage();
                MailAddress Sender = new MailAddress(ConfigurationManager.AppSettings["smtpUser"]);
                SmtpClient SmtpServer = new SmtpClient()
                {
                    Host = ConfigurationManager.AppSettings["smtpServer"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]),
                    EnableSsl = true,
                    Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["smtpUser"], ConfigurationManager.AppSettings["smtpPass"])
                };

                mail.From = Sender;
                mail.To.Add(To);
                mail.Subject = "Project Northwind";
                mail.Body = Body;

                SmtpServer.Send(mail);
                return "mail send";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
