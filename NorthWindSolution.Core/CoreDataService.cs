﻿
namespace NorthWindSolution.Core
{
    /// <summary>
    /// Base data service
    /// </summary>
    public class CoreDataService
    {
        #region Private variables
        /// <summary>
        /// Connection string to access database
        /// </summary>
        private readonly string conString = @"Data Source = DESKTOP-1ASVOVQ\SQLEXPRESS;initial catalog=Northwind;Integrated Security=True";
        private readonly string rabbitConString = @"Data Source=DESKTOP-1ASVOVQ\SQLEXPRESS;Initial Catalog=NorthwindLog;Integrated Security=True";
        #endregion

        #region Public Constructor
        public CoreDataService()
        {
            this.rabbitMQConnection = rabbitConString;
            this.ConnectionString = conString;
        }
        #endregion

        #region Properties
        public string ConnectionString { get; set; }    //Get or sets connection string
        public string rabbitMQConnection { get; set; }
        #endregion
    }
}
