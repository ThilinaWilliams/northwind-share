﻿using NorthwindSolution.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindSolution.IDataServices
{
    public interface IOrderDataService
    {
        List<Orders> SelectOrdersById(string employeeid);
        string UpdateOrders(OrderDetails orderDetails);
    }
}
