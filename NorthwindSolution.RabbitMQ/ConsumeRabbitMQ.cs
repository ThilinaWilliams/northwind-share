﻿using Dapper;
using NorthwindSolution.Logging;
using NorthWindSolution.Core;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindSolution.RabbitMQ
{
    public class ConsumeRabbitMQ: CoreDataService
    {
        /// <summary>
        /// 
        /// </summary>
        public void ConsumeQueue()
        {
            var factory = new ConnectionFactory();
            factory.Uri = new Uri("amqp://guest:guest@localhost:5672");
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();

            channel.QueueDeclare("Northwind", true, false, false);
            var consume = new EventingBasicConsumer(channel);
            consume.Received += (sender, eventArgs) =>
            {
                var message = System.Text.Encoding.UTF8.GetString(eventArgs.Body.ToArray());
                using (var connRabbit = new SqlConnection(base.rabbitMQConnection))
                {
                    try
                    {
                        connRabbit.Open();
                        var query = connRabbit.Execute("INSERT INTO LogInfo(LogInfo) VALUES(@LogInfo)", new { LogInfo = message });
                        connection.Close();
                    }
                    catch (SqlException ex)
                    {
                        NLogErrorLog.LogErrorMessages(ex.Message);
                    }
                }
            };
            channel.BasicConsume("Northwind", true, consume);
        }
    }
}
