﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindSolution.RabbitMQ
{
    public class LogRabbitMQ
    {
        /// <summary>
        /// Method to publish Message to Queue
        /// </summary>
        /// <param name="message"></param>
        public static void RabbitMQPublish(string message)
        {
            var factory = new ConnectionFactory();
            factory.Uri = new Uri("amqp://guest:guest@localhost:5672");
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();
            channel.QueueDeclare("Northwind", true, false, false, null);
            var byteMessage = System.Text.Encoding.UTF8.GetBytes(message);
            channel.BasicPublish(exchange: "", routingKey: "Northwind", null, byteMessage);
            channel.Close();
            connection.Close();
        }


    }
}
