﻿using NorthwindSolution.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindSolution.IServices
{
    public interface ICustomerBitBucketService
    {
        List<CustomerOrderBitBucket> GetCustomerOrderHistory(string CustomerID);

        List<Customers> SelectById(string customerid);

        IEnumerable<CustOrdersDetail> GetCustOrdersDetailbyId(string orderid);

        bool DeleteCustomerDetails(string CustomerID);
        List<Customers> GetAllCustomers();
        List<CustomerOrderDates> OrderDates(string custormerID);
    }
}

