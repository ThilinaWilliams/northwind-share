﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindSolution.Entities
{
    public class CustOrdersDetail
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int OrderID { get; set; }
        public string UnitPrice { get; set; }
        public string Quantity { get; set; }
        public string Discount { get; set; }
        public string ExtendedPrice { get; set; }
    }
}
