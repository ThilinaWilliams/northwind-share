﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindSolution.Entities
{
   public class CustomerOrderBitBucket
    {
        public int Total { get; set; } //Get or sets Total
        public string ProductName { get; set; } //Get or sets ProductName
    }
}
