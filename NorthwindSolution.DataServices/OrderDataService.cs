﻿using Dapper;
using NorthwindSolution.Entities;
using NorthwindSolution.Helpers;
using NorthwindSolution.IDataServices;
using NorthwindSolution.Logging;
using NorthWindSolution.Core;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindSolution.DataServices
{
    public class OrderDataService : CoreDataService, IOrderDataService
    {
        public string message;
        /// <summary>
        /// Select Orders by EmployeeID from database
        /// </summary>
        /// <param name="customerid"></param>
        /// <returns></returns>
        public List<Orders> SelectOrdersById(string employeeid)
        {
            List<Orders> orders;
            try
            {
                RabbitMQ.LogRabbitMQ.RabbitMQPublish("SelectOrdersById() API is being Called");
                using (var connection = new SqlConnection(base.ConnectionString))
                {
                    connection.Open();

                    orders = connection.Query<Orders>("SELECT * FROM Orders WHERE EmployeeID = @EmployeeID", new
                    {
                        EmployeeID = employeeid
                    }).ToList();

                    connection.Close();
                }
                return orders;
            }
            catch (Exception ex)
            {
                NLogErrorLog.LogErrorMessages(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Method to Update unitPrice and Quantity
        /// </summary>
        /// <param name="orderDetails"></param>
        /// <returns></returns>
        public string UpdateOrders(OrderDetails orderDetails)
        {
            try
            {
                using (var connection = new SqlConnection(base.ConnectionString))
                {
                    connection.Open();
                    var query = connection.Execute("UPDATE [Order Details] SET UnitPrice = @unitPrice, Quantity = @quantity WHERE ProductID = @productID AND OrderID = @orderID",
                    new
                    {
                        productID = orderDetails.ProductID,
                        orderID = orderDetails.OrderID,
                        unitPrice = orderDetails.UnitPrice,
                        quantity = orderDetails.Quantity
                    });
                    connection.Close();

                    if (query == 1)
                    {
                        Mail.sendMail("chamodshelan@gmail.com","Unit Price and Quantity has been Updated machan");
                        message = "Successfully Updated";
                    }
                    else
                    {
                        message = "Did Not Updated";
                    }
                }
            }
            catch (Exception ex)
            {
                NLogErrorLog.LogErrorMessages(ex.Message);
                throw ex;
            }
            RabbitMQ.LogRabbitMQ.RabbitMQPublish("UpdateOrders() API is Being Called");
            return message;
        }
    }
}
