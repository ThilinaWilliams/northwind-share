﻿using Dapper;
using NorthwindSolution.Entities;
using NorthwindSolution.IDataServices;
using NorthwindSolution.Logging;
using NorthWindSolution.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindSolution.DataServices
{
   public class CustomerBitBucketDataService : CoreDataService , ICustomerBitBucketDataService
    {
        /// <summary>
        /// Get Customer Order History Method 
        /// </summary>
        /// <param name="CustomerID"></param>
        /// <returns> Customer Order Stored Procedure</returns>
        public List<CustomerOrderBitBucket> GetCustomerOrderHistory(string CustomerID)
        {
            try
            {
                List<CustomerOrderBitBucket> customers;

                using (var connection = new SqlConnection(base.ConnectionString))
                {
                    connection.Open();
                    var parameters = new { CustomerID = CustomerID };
                    customers = connection.Query<CustomerOrderBitBucket>("CustOrderHist ", new { CustomerID = CustomerID }, commandType: CommandType.StoredProcedure).ToList();

                    connection.Close();

                }
                return customers;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        /// <summary>
        /// select Customers by id
        /// </summary>
        /// <param name="customerid"></param>
        /// <returns></returns>
        public List<Customers> SelectById(string customerid)
        {
            List<Customers> customers;
            try
            {
                RabbitMQ.LogRabbitMQ.RabbitMQPublish("SelectById() API is being Called");
                using (var connection = new SqlConnection(base.ConnectionString))
                {
                    connection.Open();
                    customers = connection.Query<Customers>("Select * from Customers Where CustomerID = @CustomerId", new
                    {
                        CustomerID = customerid
                    }).ToList();

                    connection.Close();
                }
                return customers;
            }
            catch (Exception ex)
            {
                NLogErrorLog.LogErrorMessages(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Get order details by id
        /// </summary>
        /// <param name="orderid"></param>
        /// <returns></returns>
        public IEnumerable<CustOrdersDetail> GetCustOrdersDetailbyId(string orderid)
        {
            try
            {
                RabbitMQ.LogRabbitMQ.RabbitMQPublish("GetCustOrdersDetailbyId() API is being Called");
                using (var connection = new SqlConnection(base.ConnectionString))
                {
                    connection.Open();

                    var parameter = new DynamicParameters();
                    parameter.Add("@OrderID", orderid);
                    return connection.Query<CustOrdersDetail>("CustOrdersDetail", parameter, commandType: CommandType.StoredProcedure);                    
                }
                
            }
            catch (Exception ex)
            {
                NLogErrorLog.LogErrorMessages(ex.Message);
                throw ex;
            }            
        }
        /// <summary>
        /// Customer Delete Data Service Method
        /// </summary>
        /// <param name="CustomerID"></param>
        /// <returns></returns>
        public bool DeleteCustomerDetails(string CustomerID)
        {
            try
            {
                bool deleted = false; ;

                using (var connection = new SqlConnection(base.ConnectionString))
                {
                    connection.Open();
                    var parameters = new { CustomerID = CustomerID };
                    var data = connection.Execute("DELETE FROM Customers WHERE CustomerID = @CustomerID", parameters);
                   // var sqlStatement = "DELETE FROM Customers WHERE CustomerID = @CustomerID";
                   // await connection.ExecuteAsync(sqlStatement, new { Id = id });
                    if (Convert.ToInt32(data) ==1)
                    {
                        deleted = true;
                    }
                    connection.Close();

                }
                return deleted;
               
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
           
        }

        // -----------------Method to Get All Customers----------------//
        public List<Customers> GetAllCustomers()
        {
            List<Customers> customers;
            using (var connection = new SqlConnection(base.ConnectionString))
            {
                try
                {
                    connection.Open();
                    customers = connection.Query<Customers>("SELECT * FROM Customers").ToList();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    NLogErrorLog.LogErrorMessages(ex.Message);
                    throw ex;
                }
            }
            RabbitMQ.LogRabbitMQ.RabbitMQPublish("GetAllCustomers() API is being Called");
            return customers;
        }

        //-------------Method to call CustOrdersOrders Stored Procedure-----------//
        public List<CustomerOrderDates> OrderDates(string customerID)
        {
            List<CustomerOrderDates> orderDates;
            using (var connection = new SqlConnection(base.ConnectionString))
            {
                try
                {
                    connection.Open();
                    orderDates = connection.Query<CustomerOrderDates>("CustOrdersOrders", new { CustomerID = customerID }, commandType: CommandType.StoredProcedure).ToList();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    NLogErrorLog.LogErrorMessages(ex.Message);
                    throw ex;
                }
                RabbitMQ.LogRabbitMQ.RabbitMQPublish("OrderDates() API is being Called");
                return orderDates;
            }

        }
    }
}
