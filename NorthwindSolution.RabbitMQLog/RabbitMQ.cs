﻿using Dapper;
using NorthWindSolution.Core;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindSolution.Logging
{
    public class RabbitMQ: CoreDataService
    {
        //-------------------------Call this method to publish message to RabbitMQ-------------//
        /// <summary>
        /// Logging Using RabbitMQ (Publish Method)
        /// </summary>
        /// <param name="message"></param>
        public void PublishToQueue(string message)
        {
            var factory = new ConnectionFactory();
            factory.Uri = new Uri("amqp://guest:guest@localhost:5672");
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();
            channel.QueueDeclare("Northwind", true, false, false, null);

            var byteMessage = System.Text.Encoding.UTF8.GetBytes(message);
            channel.BasicPublish(exchange: "", routingKey: "Northwind", null, byteMessage);

            channel.Close();
            connection.Close();
        }

        //--------------------------------Call this method to consume Message from RabbitMQ queue------------//
        /// <summary>
        /// Logging using RabbitMQ(Consume Method)
        /// </summary>
        public void Consume()
        {
            var factory = new ConnectionFactory();
            factory.Uri = new Uri("amqp://guest:guest@localhost:5672");
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();

            channel.QueueDeclare("Northwind", true, false, false);
            var consume = new EventingBasicConsumer(channel);
            consume.Received += (sender, eventArgs) =>
            {
                var message = System.Text.Encoding.UTF8.GetString(eventArgs.Body.ToArray());
                using (var connRabbit = new SqlConnection(base.rabbitMQConnection))
                {
                    try
                    {
                        connRabbit.Open();
                        var query = connRabbit.Execute("INSERT INTO LogInfo(LogInfo) VALUES(@LogInfo)", new { LogInfo = message });
                        connection.Close();
                    }
                    catch (SqlException ex)
                    {
                        NLogErrorLog.LogErrorMessages(ex.Message);
                    }
                }
            };
            channel.BasicConsume("Northwind", true, consume);

        }
    }
}
