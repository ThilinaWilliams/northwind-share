﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindSolution.Logging
{
    public class NLogErrorLog
    {

        #region Variable Declared
        private static Logger error = LogManager.GetLogger("ErrorRule");
        private static Logger info = LogManager.GetLogger("InfoRule");
        #endregion

        #region Public Methods
        //----------Use this method to Error Log-----------//
        public static void LogErrorMessages(string message)
        {
            error.Error(message);
        }
        //----------Use this method to Infor Log-----------//
        public static void LogInfoMessage(string message)
        {
            info.Info(message);
        } 
        #endregion
    }
}
