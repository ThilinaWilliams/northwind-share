﻿using NorthwindSolution.Entities;
using NorthwindSolution.IDataServices;
using NorthwindSolution.IServices;
using NorthwindSolution.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindSolution.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderDataService dataService;
        private readonly IOrderDataService updateDataService;
        public OrderService(IOrderDataService dataService, IOrderDataService updateDataService)
        {
            this.dataService = dataService;
            this.updateDataService = updateDataService;
        }

        /// <summary>
        /// Call SelectOrdersById method to select orders by EmployeeID
        /// </summary>
        /// <param name="customerid"></param>
        /// <returns></returns>
        public List<Orders> SelectOrdersById(string employeeid)
        {
            try
            {                
                return dataService.SelectOrdersById(employeeid);
            }
            catch (Exception ex)
            {
                NLogErrorLog.LogErrorMessages(ex.Message);
                throw ex;
            }
        }

        public string UpdateOrders(OrderDetails orderDetails)
        {
            try
            {
                return updateDataService.UpdateOrders(orderDetails);
            }
            catch (Exception ex)
            {
                NLogErrorLog.LogErrorMessages(ex.Message);
                throw ex;
            }

        }
    }
}
