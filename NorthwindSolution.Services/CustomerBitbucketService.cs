﻿using NorthwindSolution.Entities;
using NorthwindSolution.Helpers;
using NorthwindSolution.IDataServices;
using NorthwindSolution.IServices;
using NorthwindSolution.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindSolution.Services
{
    /// <summary>
    /// Service 
    /// </summary>
    public class CustomerBitbucketService : ICustomerBitBucketService
    {
        #region Private variables
        private readonly ICustomerBitBucketDataService dataService;

        #endregion

        #region Public Constructor
        public CustomerBitbucketService(ICustomerBitBucketDataService dataService)
        {
            this.dataService = dataService;
        }
        #endregion
        public List<CustomerOrderBitBucket> GetCustomerOrderHistory(string CustomerID)
        {
            try
            {
                return dataService.GetCustomerOrderHistory(CustomerID);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Select customers by id
        /// </summary>
        /// <param name="customerid"></param>
        /// <returns></returns>
        public List<Customers> SelectById(string customerid)
        {
            try
            {
                return dataService.SelectById(customerid);
            }
            catch (Exception ex)
            {
                NLogErrorLog.LogErrorMessages(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Get order details by id
        /// </summary>
        /// <param name="orderid"></param>
        /// <returns></returns>
        public IEnumerable<CustOrdersDetail> GetCustOrdersDetailbyId(string orderid)
        {
            try
            {
                return dataService.GetCustOrdersDetailbyId(orderid);
            }
            catch (Exception ex)
            {
                NLogErrorLog.LogErrorMessages(ex.Message);
                throw ex;
            }
        }
        /// <summary>
        /// Customer Delete  Service Method
        /// </summary>
        /// <param name="CustomerID"></param>
        /// <returns></returns>
        public bool DeleteCustomerDetails(string CustomerID)
        {
            try
            {
                bool success = false;
                success = dataService.DeleteCustomerDetails(CustomerID);

                if (success)
                {
                    Mail.sendMail("williamsthilina@gmail.com", "Delete Details Succesfully");
                }

                return success;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //------------------------Get All Customers Data service method--------------//
        public List<Customers> GetAllCustomers()
        {
            try
            {
                return dataService.GetAllCustomers();
            }
            catch (Exception ex)
            {
                NLogErrorLog.LogErrorMessages(ex.Message);
                throw ex;
            }
        }

        public List<CustomerOrderDates> OrderDates(string customerID)
        {
            try
            {
                return dataService.OrderDates(customerID);
            }
            catch (Exception ex)
            {
                NLogErrorLog.LogErrorMessages(ex.Message);
                throw ex;
            }
        }

    }
}
